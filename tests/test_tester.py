import pytest

from pytest_plugin_helpers.tester import isolate


@pytest.fixture
def local_fixture_1():
    return True


@isolate
def test_simple():
    assert True


@isolate
def test_fixture_from_same_file(local_fixture_1):
    assert local_fixture_1 is True


@isolate
def test_fixture_from_same_level_conftest(fixture_from_contest_1):
    assert fixture_from_contest_1 is True


@isolate
@pytest.mark.skip(reason="Test not implemented yet")
def test_fixture_from_same_file_relative_imports():
    pass  # TODO
