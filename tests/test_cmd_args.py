from pytest_plugin_helpers.tester import isolate


@isolate
def test_cmd_args_default(request):
    assert request.config.getoption("--capture") == "fd"


@isolate(cmd_args="-s")
def test_cmd_args(request):
    assert request.config.getoption("--capture") == "no"
