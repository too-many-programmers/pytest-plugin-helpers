=====================
pytest-plugin-helpers
=====================

.. image:: https://img.shields.io/pypi/v/pytest-plugin-helpers.svg
    :target: https://pypi.org/project/pytest-plugin-helpers
    :alt: PyPI version

.. image:: https://img.shields.io/pypi/pyversions/pytest-plugin-helpers.svg
    :target: https://pypi.org/project/pytest-plugin-helpers
    :alt: Python versions

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
    :target: https://github.com/psf/black


A plugin to help developing and testing other plugins.

----

This `pytest`_ plugin was generated with `Cookiecutter`_ along with `@hackebrot`_'s `cookiecutter-pytest-plugin`_ template.


Features
--------

* TODO


Requirements
------------

* TODO


Installation
------------

You can install "pytest-plugin-helpers" via `pip`_ from `PyPI`_::

    $ pip install pytest-plugin-helpers


Usage
-----

* TODO

Contributing
------------
Contributions are very welcome. Tests can be run with `tox`_, please ensure
the coverage at least stays the same before you submit a pull request.

License
-------

Distributed under the terms of the `MIT`_ license, "pytest-plugin-helpers" is free and open source software


Issues
------

If you encounter any problems, please `file an issue`_ along with a detailed description.

.. _`file an issue`: https://gitlab.com/too-many-programmers/pytest-plugin-helpers/issues
.. _`Cookiecutter`: https://github.com/audreyr/cookiecutter
.. _`@hackebrot`: https://github.com/hackebrot
.. _`MIT`: http://opensource.org/licenses/MIT
.. _`cookiecutter-pytest-plugin`: https://github.com/pytest-dev/cookiecutter-pytest-plugin
.. _`pytest`: https://github.com/pytest-dev/pytest
.. _`tox`: https://tox.readthedocs.io/en/latest/
.. _`pip`: https://pypi.org/project/pip/
.. _`PyPI`: https://pypi.org/project
